
const child_process = require('child_process')
const fs = require('fs')
const path = require('path')
const util = require('util')
const {dialog} = require('electron').remote
const _readdir = util.promisify(fs.readdir)
const _stat = util.promisify(fs.stat)
const _mkdir = util.promisify(fs.mkdir)
const _unlink = util.promisify(fs.unlink)

const [ffmpeg, gm, pngquant] = ((...args) => {
  let base = __dirname.replace('app.asar', 'app.asar.unpacked')
  let platformFolder = {'darwin':'mac','win32':'win','linux':'linux'}[process.platform]
  let exePath = exe => path.join(base, 'build', 'platform', platformFolder, platformFolder == 'win' ? exe+'.exe' : exe)
  return args.map(exePath)
})('ffmpeg', 'gm', 'pngquant')


// execute and pipe binaries, e.g.: _exec('curl example.com', 'cat')
let exec = (...commands) => new Promise((fulfill, reject) => {
    // spawn all processes at once in a for loop and connect their stdout to the stdings
    let processes = [], errOut = []
    for(let i = 0 ; i < commands.length ; i++) {
      process.stdout.write(commands[i]+'\n')
      let [program, ...args] = commands[i].split(' ')
      // start process and connect its input to the previous process' output
      processes[i] = child_process.spawn(
        program, args, {stdio: [(i == 0 ? 'ignore' : 'pipe'), 'pipe', 'pipe']})
      if(i > 0) processes[i-1].stdout.pipe(processes[i].stdin)
      // capture error out
      errOut[i] = Buffer.alloc(0)
      processes[i].stderr.on('data', d => errOut[i] = Buffer.concat([errOut[i], d]))
      // log errors and stop all processes when any of them errors
      processes[i].on('error', e => {
        console.log(e)
        processes.forEach(p => p.kill())
      })
      processes[i].on('close', code => {
        if(code && errOut[i].length)
          console.warn(path.basename(program) + ': ' + errOut[i].toString())
      })
    }
    let stdOut = Buffer.alloc(0)
    processes[commands.length-1].stdout.on('data', d => stdOut = Buffer.concat([stdOut, d]))
    processes[commands.length-1].on('close', code => fulfill({code, stdOut, errOut}))
  })
let isOpaque = async file => {
  let {stdOut} = await exec(`${gm} convert -size 1 ${file} -channel opacity -verbose info:-`)
  return parseFloat(stdOut.toString().match(/Maximum:.*\((.*)\)/)[1]) == 0
}

window.onload = () => {
  // preview
  {
    let thumbnail = document.querySelector('#thumbnail > img')
    let setThumbnail = async file => {
      thumbnail.className = 'waiting'
      document.querySelector('#thumbnail > p').innerText = 'converting ⏲'
      let generatePng = exec(
        `${gm} convert -size 600x600 ${file} -scale 600x600^ -gravity Center -crop 600x600+0+0 png:-`,
        `${pngquant} -` )
      let generateJpg = await isOpaque(file)
        ? exec(`${gm} convert -size 600x600 ${file} -scale 600x600^ -gravity Center -crop 600x600+0+0 jpg:-`)
        : null
      let [pngout, jpgout] = await Promise.all([generatePng, generateJpg])
      if(jpgout && jpgout.stdOut.length < pngout.stdOut.length) {
        thumbnail.format = 'jpg'
        thumbnail.buffer = jpgout.stdOut
        thumbnail.src = 'data:image/jpeg;base64,' + jpgout.stdOut.toString('base64')
      } else {
        thumbnail.format = 'png'
        thumbnail.buffer = pngout.stdOut
        thumbnail.src = 'data:image/png;base64,' + pngout.stdOut.toString('base64')
      }
    }
    thumbnail.ondragover = e => e.preventDefault()
    thumbnail.ondrop = e => {
      e.preventDefault()
      if(e.dataTransfer.files[0]) setThumbnail(e.dataTransfer.files[0].path)
    }
    thumbnail.onclick = e => {
      let file = dialog.showOpenDialog({properties: ['openFile']})
      if(file) setThumbnail(file[0])
    }
    thumbnail.onload = () => thumbnail.className = ''

    document.querySelector('#download').onclick = () => {
      let link = document.createElement('a')
      let data = {
        name: document.querySelector('#name').innerText,
        minPlayer: parseInt(document.querySelector('#minPlayer').innerText),
        maxPlayer: parseInt(document.querySelector('#maxPlayer').innerText),
        targetGroup: document.querySelector('#targetGroup').innerText,
        version: document.querySelector('#version').innerText,
        table: document.querySelector('#table').innerText,
        lang: document.querySelector('#lang').innerText,
        shorttext: document.querySelector('#shorttext').innerText,
        thumbnail: 'preview.'+thumbnail.format
      }
      let folder = dialog.showOpenDialog({properties: ['openDirectory']})
      if(folder) {
        fs.writeFileSync(path.join(folder[0], 'preview.json'), JSON.stringify(data, null, '  '))
        fs.writeFileSync(path.join(folder[0], 'preview.'+thumbnail.format), thumbnail.buffer, 'buffer')
      }
    }
  }


  // asset compression
  {
    let pane = document.querySelector('#compress')
    let state = {status: 'empty'}
    // debug
    window.state = state
    let scanAssets = async folder => {
      let flattenDir = async (dir) => {
        try {
          let filesAndFolders = await _readdir(dir)
          let foldersExpanded = await Promise.all(filesAndFolders.map(file => flattenDir(path.join(dir,file))))
          // flatten
          return [].concat(...foldersExpanded);
        } catch(e) { if(e.code == 'ENOTDIR') return [dir]; else if(e.code == 'ENOENT') return []; else throw e }
      }
      let files = (await flattenDir(folder)).filter(f => /[.](jpg|png|mp3|wav|flac|opus)$/.test(f));
      let stats = await Promise.all(files.map(f => _stat(f)))
      return files.map((_,i) => ({path:files[i], stat:stats[i]}))
    }
    let renderPane = (pane, state) => {
      pane.className = state.status
      let formatSize = size => {
        let exp = Math.floor(Math.log(10*size) / Math.log(1024))
        return Math.floor((10*size) / 1024**exp)/10 + ['','K','M','G'][exp]
      }
      let renderRate = asset => {
        let rate = state.cache[asset.path] ? state.cache[asset.path].stat.size/asset.stat.size - 1 : null
        return `<td class="rate ${rate!=null?(rate>0?'bad':(rate>-.05?'neutral':'good')):''}">
                  ${rate!=null ? (rate>0?'+':'') + Math.round(1000 * rate) / 10 + '%' : ''}
                </td>`
      }
      let renderRename = asset => {
        let destExt = state.cache[asset.path] && path.extname(state.cache[asset.path].path), srcExt = path.extname(asset.path)
        return destExt != srcExt && destExt
      }
      let renderAsset = asset => `<tr>
          <td>${path.relative(state.src, asset.path)}</td>
          <td class="size" style="font-size:${Math.log(asset.stat.size)*.05}em;">${formatSize(asset.stat.size)}</td>
          <td class="status ${state.finished[asset.path]}" ${state.errors[asset.path] ?
            `error="${state.errors[asset.path]}"` : ''}></td>
          ${renderRate(asset)}
          <td class="rename">${renderRename(asset) || ''}</td>
        </tr>`
      pane.innerHTML = ''
      switch(state.status) {
        case 'empty': pane.innerHTML += '<p>Drag folder with assets into this box.</p>'; break;
        case 'loading': pane.innerHTML += `<p>${state.src} ⏲</p>`; break;
        case 'finished':
          let srcSize = state.assets.reduce((a,x) => a+x.stat.size, 0)
          let destSize = state.assets.reduce((a,x) => a+(state.cache[x.path] && state.cache[x.path].stat.size || x.stat.size), 0)
          pane.innerHTML += `<h4>${formatSize(srcSize)} → ${formatSize(destSize)}</h4>`
        case 'ready':
        case 'compressing':
          pane.innerHTML += `<p>${state.src}</p><table>${state.assets.map(renderAsset).join('')}</table>`;
          break;
        default: console.error('Invalid pane status: '+state.status);
      }
    }
    let setFolder = folder => {
      renderPane(pane, Object.assign(state, {
        status: 'loading',
        src: folder,
        dest: path.join(path.dirname(folder), 'compressed')
      }))
      ;(async()=>{
        let [assets, cacheArr] = await Promise.all([scanAssets(state.src), scanAssets(state.dest)])
        assets.sort((x,y) => y.stat.size - x.stat.size)
        let cache = {}, finished = {}
        for(let a of cacheArr) {
          let src = assets.find( _a => _a.path.replace(/[.][^.]+/, '')
            == path.join(state.src, path.relative(state.dest, a.path)).replace(/[.][^.]+/, '') )
          if(src && src.stat.mtimeMs < a.stat.mtimeMs) {
            cache[src.path] = a
            finished[src.path] = 'finished'
          }
        }
        renderPane(pane, Object.assign(state, {status:'ready', assets, cache, finished, errors:{}}))
      })()
    }
    pane.ondragover = e => e.preventDefault()
    pane.ondrop = e => {
      e.preventDefault()
      if(e.dataTransfer.files[0])
        setFolder(e.dataTransfer.files[0].path)
    }
    pane.onclick = () => {
      if(state.status == 'empty') {
        let folder = dialog.showOpenDialog({properties: ['openDirectory']})
        if(folder) setFolder(folder[0])
        return
      }
      // compress sequentially to reduce memory+cpu load
      if(state.status != 'ready') return
      let destPath = (srcPath, ext=null) => {
        let res = path.join(state.dest, path.relative(state.src, srcPath))
        return ext ? res.replace(/[.][^.]+$/, ext) : res
      }
      let mkdirRecursive = async (folder, root) => {
        let chain = path.relative(root, folder).split(/[\\/]/).concat('')
        for(let node=root ; chain.length ; node=path.join(node, chain.shift())) {
          try{ await _mkdir(node) }
          catch(e) { if(e.code != 'EEXIST') console.log(e) }
        }
      }
      renderPane(pane, Object.assign(state, {status:'compressing'}))
      ;(async()=>{
        for(a of state.assets) {
          // cache lookup
          if(state.finished[a.path]) continue;

          state.finished[a.path] = 'pending'
          renderPane(pane, state)
          await mkdirRecursive(path.dirname(destPath(a.path)), state.dest)
          let code, stdOut, errOut, destFile
          switch(path.extname(a.path)) {
            case '.jpg':
              ( {code, stdOut, errOut} = await exec(`${gm} convert ${a.path} -quality 75 ${destFile=destPath(a.path)}`) )
              break;
            case '.png':
              // consider jpg if image is opaque
              let jpgGen = await isOpaque(a.path) ? exec(`${gm} convert ${a.path} -quality 75 ${destPath(a.path,'.jpg')}`) : null
              let pngGen = exec(`${pngquant} ${a.path} -o ${destPath(a.path)}`)
              let [jpg, png] = await Promise.all([jpgGen, pngGen])
              // delete both files on any error
              if(png.code || jpg && jpg.code) {
                ( {code, stdOut, errOut} = png.code && png || jpg.code && jpg )
                if(!png.code) _unlink(destPath(a.path))
                if(jpg && !jpg.code) _unlink(destPath(a.path,'.jpg'))
              } else if(jpg && (await _stat(destPath(a.path,'.jpg'))).size < (await _stat(destPath(a.path))).size) {
                destFile = destPath(a.path,'.jpg')
                ; ( {code, stdOut, errOut} = jpg )
                _unlink(destPath(a.path))
              } else {
                destFile = destPath(a.path)
                ; ( {code, stdOut, errOut} = png )
                if(jpg) _unlink(destPath(a.path,'.jpg'))
              }
              break;
            case '.wav':
            case '.mp3':
            case '.opus':
            case '.flac':
              ( {code, stdOut, errOut} = await exec(`${ffmpeg} -i ${a.path} -c:a libopus -b:a 24k ${destFile=destPath(a.path,'.opus')}`) )
              break;
          }
          state.finished[a.path] = code ? 'error' : 'finished'
          if(code)
            state.errors[a.path] = errOut
          else
            state.cache[a.path] = {path:destFile, stat:await _stat(destFile)}
          renderPane(pane, state)
        }
        state.status = 'finished'
        renderPane(pane, state)
      })()
    }
    renderPane(pane, state)
  }
}

