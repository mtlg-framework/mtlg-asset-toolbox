
# Asset Tool

A tool to create a game preview and to optimize image-, sound- and videoassets.

This tool is rather heavyweight (~100MB) because of the dependencies.

Download for your platform: [Mac](https://gitlab.com/mtlg-framework/mtlg-asset-toolbox/raw/dist/dist/mtlg-asset-toolbox.zip), [Windows](https://gitlab.com/mtlg-framework/mtlg-asset-toolbox/raw/dist/dist/mtlg-asset-toolbox.exe), [Linux](https://gitlab.com/mtlg-framework/mtlg-asset-toolbox/raw/dist/dist/mtlg-asset-toolbox.AppImage)

## Development

Install git and nodejs. Install git-lfs if not available (check with `git-lfs` or `git lfs`).

The asset tool uses graphicsmagick, ffmpeg and pngquant to convert and compress assets. These tools are distributed as binaries and stored in the git repository as lfs files under `build/platform/{linux,mac,win}`. They are ~70MB in size per platform. If bandwidth is low you can clone the repository without pulling those binaries with following commands:

```
GIT_LFS_SKIP_SMUDGE=1 git clone git@gitlab.com:mtlg-framework/mtlg-asset-toolbox.git
cd mtlg-asset-toolbox
```

The app uses electron as frontend. To install electron, pull the binaries for your specific platform and start the program, use:

```
npm install # or faster: `yarn`
git-lfs pull -I build/platform/win
# git-lfs pull -I build/platform/linux
# git-lfs pull -I build/platform/mac
electron .
```


### Modification

The asset-tool consists of following parts:

| file/folder           | function
| ---                   | ---
| build/icon.png        | App icon that is shown on Desktop/Tray/etc.
| build/platform        | ffmpeg, gm and pngquant binaries for each platform
| dist                  | packaged executables will be placed here (gitignored)
| test                  | set of assets used to test the program
| node_modules          | electron (folder exists after running `npm install`)
| index.html, style.css | frontend static part
| frontend.js           | frontend functionality
| main.js               | electron app (just 15 lines of code)
| package.json          | packaging configuration

Optionally uncomment the line

```
// win.webContents.openDevTools()
```

in `main.js` to have the web-developer tools open when starting the app.


### Distribute

To build the program for distribution:

```
# download all remaining binaries if not already done
git-lfs pull

# checkout dist, skipping download of previous packages
GIT_LFS_SKIP_SMUDGE=1 git checkout dist
git merge master

# create package for linux, macos and windows
npm run linux && npm run macos && npm run win

# upload
git add -f dist/mtlg-asset-toolbox.AppImage
git add -f dist/mtlg-asset-toolbox.exe
git add -f dist/mtlg-asset-toolbox.zip
git commit -m 'upload'
git push
```


## Dependencies

- [electron 3.0.13](https://github.com/electron/electron/releases) (npm download)
- [pngquant 2.12](https://pngquant.org/) (win&macos binary downloads, linux source download)
- [ffmpeg 4.0.1](https://ffmpeg.org/download.html) (all binary downloads)
- [GraphicsMagick 7.0.8](http://www.graphicsmagick.org/download.html) (win binary download, linux&macos source download)

Source builds (static):

- pngquant on linux (debian:8 docker image) (5' download+build, needs ~60MB storage)

  ```sh
  git clone --recursive https://github.com/kornelski/pngquant.git
  cd pngquant
  curl -L http://zlib.net/zlib-1.2.11.tar.gz | tar xz
  curl -L https://downloads.sourceforge.net/project/lcms/lcms/2.9/lcms2-2.9.tar.gz | tar xz
  curl -L https://download.sourceforge.net/libpng/libpng-1.6.35.tar.gz | tar xz
  (cd zlib-*           && ./configure --static                             && make install)
  (cd libpng-*         && ./configure --disable-shared                     && make)
  (cd lcms2-*          && ./configure --disable-shared                     && make)
  ./configure && make
  ./pngquant --version
  cp ./pngquant ..

  # test
  ./pngquant test/img/test.png
  ```
- GraphicsMagick on linux (debian:8 docker image) (~5' download+build, needs ~100MB storage):

  ```sh
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/1.3/GraphicsMagick-1.3.31.tar.gz | tar xvz
  # zlib needed for png
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/delegates/zlib-1.2.11.tar.gz | tar xvz
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/delegates/libpng-1.6.28.tar.gz | tar xvz
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/delegates/libwebp-1.0.0.tar.gz | tar xvz
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/delegates/jpegsrc.v6b2.tar.gz | tar xvz
  # use less zlib-<TAB>/configure to discover configure options
  (cd zlib-*           && ./configure --static                             && make install)
  (cd libpng-*         && ./configure --disable-shared                     && make install)
  (cd libwebp-*        && ./configure --disable-shared --enable-libwebpmux && make install)
  (cd jpeg-*           && ./configure --disable-shared                     && make install)
  (cd GraphicsMagick-* && ./configure --disable-installed                  && make install)
  gm version
  ldd /usr/local/bin/gm
  cp /usr/local/bin/gm .

  # test
  mkdir test && cd test
  cp ../jpeg-6b2/testimg.bmp ./test.bmp
  gm convert test.bmp jpg.jpg
  gm convert jpg.jpg jpg.bmp
  gm convert test.bmp png.png
  gm convert png.png png.bmp
  gm convert test.bmp webp.webp
  gm convert webp.webp webp.bmp
  ```
- GraphicsMagick on mac (folder-local install) (~5' download+build, needs ~100MB storage):

  ```
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/1.3/GraphicsMagick-1.3.31.tar.gz | tar xvz
  # zlib needed for png
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/delegates/zlib-1.2.11.tar.gz | tar xvz
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/delegates/libpng-1.6.28.tar.gz | tar xvz
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/delegates/libwebp-1.0.0.tar.gz | tar xvz
  curl -L http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/delegates/jpegsrc.v6b2.tar.gz | tar xvz

  # use less zlib-<TAB>/configure to discover configure options
  export mydir=$(pwd)
  export CPPFLAGS="-I$mydir/include"
  export LDFLAGS="-L$mydir/lib"
  (cd zlib-*           && ./configure --static                             --prefix=$mydir && make install)
  (cd libpng-*         && ./configure --disable-shared                     --prefix=$mydir && make install)
  (cd libwebp-*        && ./configure --disable-shared --enable-libwebpmux --prefix=$mydir && make install)
  (cd jpeg-*           && ./configure --disable-shared                     --prefix=$mydir && make install)
  (cd GraphicsMagick-* && ./configure --disable-installed                  --prefix=$mydir && make install)
  ./bin/gm version
  ldd ./bin/gm

  # test
  mkdir test && cd test
  cp ../jpeg-6b2/testimg.bmp ./test.bmp
  ../bin/gm convert test.bmp jpg.jpg
  ../bin/gm convert jpg.jpg jpg.bmp
  ../bin/gm convert test.bmp png.png
  ../bin/gm convert png.png png.bmp
  ../bin/gm convert test.bmp webp.webp
  ../bin/gm convert webp.webp webp.bmp
  ```

- GraphicsMagick on windows, installed from binary (Q8, 64bit) and stripped down to the required files:

  ```
  wget https://downloads.sourceforge.net/project/graphicsmagick/graphicsmagick-binaries/1.3.31/GraphicsMagick-1.3.31-Q8-win64-dll.exe
  wine GraphicsMagick*
  cp extract/*.dll extract/*.mgk extract/dcraw.exe extract/gm.exe extract/*.manifest dist
  ```
