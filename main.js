const { app, BrowserWindow } = require('electron')

let win

function createWindow () {
  win = new BrowserWindow({ width: 800, height: 600 })
  win.loadFile('index.html')
  win.on('closed', () => win = null)
  // debug
  // win.webContents.openDevTools()
}

app.on('ready', createWindow)
app.on('window-all-closed', () => process.platform !== 'darwin' && app.quit())
app.on('activate', () => win === null && createWindow())
